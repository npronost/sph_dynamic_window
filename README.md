This project is build from the SPlisHSPlasH framework:
https://github.com/InteractiveComputerGraphics/SPlisHSPlasH

Modification are made to implement the DFSPH algorithm on GPU.

This project requires CUDA in top of the requirements of the original project.

Note: this project still uses the mass-based implementation and not the volume-based implementation currently used in the SPlisHSPlasH project for the multi-fluid system.

The GPU implementation in this repository contains multiple optimizations that can be activated/deactivated through the use of the #defines in the files DFSPH_define_c.h and DFSPH_define_cuda.h

This project also contains the following systems that can be activated inside the step function in the DFSPH_CUDA class:

	- an implementation of a special initialization system to start a simulation with a fluid at rest for any shape of boundary
		See the RestFluidLoader.h file to explore this system

	- a "simple" open boundary method that can absorb any wave generated inside the simulated area
		See the OpenBoundarySimple.h file to explore this system

	- a dynamic window that can be used to move the simulated area to follow a subject of interest (e.g. a boat)
		See the DynamicWindow.h file to explore this system
		
Installation:
This project requires having CUDA installed on top of the dependencies from the SPlisHSPlasH framework.

For storage space reasons the "save_folder" that should contain the scene saved for loading purpose is not included in the repository.
As such to ensure proper functioning of some components it is required to create a folder named "save_folder" in the "data" folder. 
If you want to rename this folder you may do it in the constructor of the DFSPHCData class.
